# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 10:34:21 2016

@author: Matthew
"""

import threading
import time
import psutil

class Retrieval():
    """"Base class for this file. Contains list for data obtainted
    
    Functions
    ----------
    
    startRetrieval - start the collections of the data. Takes delay, for the time between each data point, and loops, for the number of itterations. Returns array of tuples for utilization.
    """
    currentData = []

    class Utilization(threading.Thread):
        """
        Class for thread when taking utilization percentages
        
        Inherits from threading.Thread for thread management
        
        Functions
        ----------
        
        __init__ - only for initiating the thread
        
        run - run included as part of the thread, yields CPU, RAM and disk utils, as percents of total
        """
        def __init__(self):
            threading.Thread.__init__(self)
             
        def run(self):
#            print(psutil.cpu_percent(),
#                  psutil.virtual_memory().percent,
#                  (psutil.disk_io_counters().write_time + psutil.disk_io_counters().read_time - Retrieval.Timing.lastDiskTime)/(10000*Retrieval.Timing.diskNo))
                  
            Retrieval.currentData.append((psutil.cpu_percent(),
                                          psutil.virtual_memory().percent,
                                          (psutil.disk_io_counters().write_time + psutil.disk_io_counters().read_time - Retrieval.Timing.lastDiskTime)/(1000*Retrieval.Timing.diskNo)))
            
            Retrieval.Timing.lastDiskTime = psutil.disk_io_counters().write_time + psutil.disk_io_counters().read_time
            
    class Timing(threading.Thread):
        """Class for the thead for the management of the timing between recording the data. Also manages total disk time.
        
        Inhertis from threading.Thread for thread managment
        
        Functions
        ----------
        
        __init__ - initates the thread and sets the time between data and the no. of itterations
        
        run - part of inheritance, overwritten to manage disk times and the looped calling of the data appending        
        """
        lastDiskTime = 0
        diskNo = len(psutil.disk_io_counters(perdisk = True))

        def __init__(self,delay,loops):
            threading.Thread.__init__(self)
            self.delay = delay
            self.loops = loops
            
        def run(self):
            Retrieval.Timing.lastDiskTime = psutil.disk_io_counters().write_time + psutil.disk_io_counters().read_time
            
            for i in range(0,self.loops):
                u = Retrieval.Utilization()
                u.start()
                time.sleep(self.delay)
                u.join()
    
    def startRetrieval(delay,loops,notes):           
        Retrieval.currentData = []
        date = time.strftime("%d/%m/%Y %H:%M:%S")
        t = Retrieval.Timing(delay,loops)
        t.start()
        t.join()
        return {"data":Retrieval.currentData,
                "delay":delay,
                "loops":loops,
                "memory":psutil.virtual_memory().total,
                "notes":notes,
                "date":date,
                "graphicsPresent":False}
        
#print(Retrieval.startRetrieval(1,10,"test"))