# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 09:58:34 2016

@author: matti_000
"""

import sys
from PyQt4 import QtGui
from LivePlot import plotGraph

class GraphDiag(QtGui.QMainWindow):
    
    def __init__(self, parent = None):
        super(GraphDiag,self).__init__(parent)
        QtGui.QWidget.__init__(self, parent)
        
        self.body = Body(self)
        
        self.setCentralWidget(self.body)
        
    def openGraph(graphType,graphData):
        if graphType == "Live":
            if graphData == "CPU":
                plotGraph(0)
            elif graphData == "RAM":
                plotGraph(1)
            elif graphData == "Disk":
                plotGraph(0)
            else:
                print("nop tu geepeeyou")
                
        if graphType == "Data":
            if Data == None:
                print("No Data Loaded")
                return
            else:
                if graphData == "CPU":
                    print("CPU Graph - Data")
                elif graphData == "RAM":
                    print("RAM Graph - Data")
                elif graphData == "Disk":
                    print("Disk Graph - Data")
                else:
                    print("nop tu geepeeyou")

class Body(QtGui.QWidget):
    
    def __init__(self, parent = None):
        super(Body,self).__init__(parent)
        
        self.initUI()
    
    def initUI(self):
        
        grid = QtGui.QGridLayout()
        self.setLayout(grid)
        grid.setSpacing(10)
        
        graphTypeHead = QtGui.QLabel("Graph Type")
        graphTypeHead.setFixedHeight(12)
        
        grid.addWidget(graphTypeHead,0,0)
        
        graphType = QtGui.QComboBox(self)
        graphType.addItems(["Data","Live"])
        
        grid.addWidget(graphType,1,0)
        
        graphDataHead = QtGui.QLabel("Graph Data")
        graphDataHead.setFixedHeight(12)
        
        grid.addWidget(graphDataHead,0,1)
        
        graphData = QtGui.QComboBox(self)
        graphData.addItems(["CPU","RAM","Disk","GPU"])
        
        grid.addWidget(graphData,1,1)
        
        startGraphButton = QtGui.QPushButton("Create Graph!", self)
        startGraphButton.setShortcut("Enter")
        startGraphButton.clicked.connect(lambda: GraphDiag.openGraph(graphType.currentText(),graphData.currentText()))
        
        grid.addWidget(startGraphButton,2,0,1,2)
        
        self.show()
            

        
def main():
    
    app = app = QtGui.QApplication(sys.argv)
    ex = GraphDiag()
    sys.exit(app.exec_())
    
if __name__ == "__main__":
    main()