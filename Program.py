# -*- coding: utf-8 -*-

import time
import threading
import random
import tkinter
import numpy
import platform
import psutil
from pynvml import *

class Main(threading.Thread):
    """Class for main program body; GUI and graphing
    
    Functions
    ----------
    run - for threading; ran when thread is started
    
    startRetrival - called for "start" button in "main" Tk window
    """
    def __init__(self):
        threading.Thread.__init__(self)
        self.defautDelay = 1
        
    def run(self):
        """Main GUI
        Initiation of the main GUI from starting thread
        
        Arguments
        ----------
        None
        
        Returns
        ----------
        None
        
        Modules
        ----------
        Tkinter - for GUI
        
        threading - run function for when thread is started
        
        Tk Widgets
        ----------
        delay: Entry for the time delay between taking data
        
        start: Button to start data retrival
        """
            
        main = tkinter.Tk()
        main.wm_title("Bench.py")
        delay = tkinter.Entry(main, text = "Delay (seconds):")
        delay.grid(row=0,column=0)
        start = tkinter.Button(main, text = "Start", 
                               command = lambda: Main.startRetrival(
                               delay.get(),Data.gpuArray,Data.gpuUtilAvailablity)).grid(row=1,column=0)
        main.mainloop()
        
    def startRetrival(delay):
        """Command for start button in main GUI window
        Opens temporary Tk window during data collection
        Creates an error if the inputted values aren't floats
        Initiates Data thread for retriving data
        
        Tk Widgets
        ----------
        infoWindow - info: Label declaring that the data is being colected with the specifed delay

        error - errorMessage: Label describing the error
                errorClose:   Button that closes error message
        """
        
        try:
            delay = float(delay)

        except ValueError:
            error = tkinter.Tk()
            errorMessage = tkinter.Label(error, text = "Invalid Input").grid(row=0,column=0)
            errorClose = tkinter.Button(error, text = "Close", command = error.destroy).grid(row=1,column=0)
            error.mainloop()
        
        infoWindow = tkinter.Tk()
        infoWindow.wm_title("")
        info = tkinter.Label(infoWindow, text = "Retriving data with "+str(delay)+" sec delay").grid(row=0,column=0)
        d = Data(delay)
        d.start()
        infoWindow.mainloop()
        
class Data(threading.Thread):
    """Class for collection of data and system information
    
    Functions
    ----------
    run - run function for when thread is started
    
    retriveData - currently a dummy for  data collection
    
    getGpu - gets system's gpu information
    
    getSystem - gets certian system variables
    """
    gpuArray = 0
    systemArray = 0
    
    def __init__(self,delay):
        threading.Thread.__init__(self)
        self.delay = delay
        self.completedRetrival = False
        
    def run(self):
        """run for the Data thread; meant to be used when data collection is initiated
        
        Arguments
        ----------
        self.delay - delay from the main UI window
        
        
        """
        Data.retriveData(self.delay)
        
    def retriveData(delay,gpuArray):
        """Currently a dummy data collection func
        
        Needs pynvml data
        
        Needs to work for up to 4 Nvidia GPUs (max SLI count of GPUs)
        
        Arguments
        ----------
        delay - the delay between collections of data
        
        gpuArray - the array of information about installed Nvidia GPUs
        
        Returns
        ----------
        dataArray - numpy array with all data from data retrival
        
        Modules
        ----------
        psutil - for system utilization
        
        pynvml - for Nvidia GPU utilization
        
        time - for delays between data recording
        
        numpy - for returning data in a 2D array
        
        Tk Widgets
        ----------
        None
        """
        demoArray = []
        for i in range(10):
            demoArray.append(random.randint(0,10))
            time.sleep(delay)
            print(demoArray[i])
            
    def getGpu():
        """Gets GPU name, handle, whether utilization rates are supported and total VRAM
        
        Arguments
        ----------
        None
        
        Returns
        ----------
        2D numpy array with all variables, per device
        
        Modules
        ----------
        numpy - for creating a 2D arry for returning data
        
        pynvml - for GPU varibales
        
        Tk Widgets
        ----------
        None        
        """
        tempGpuName = []
        tempGpuArray = []
        tempGpuUtilAvailablity = []
        tempGpuVram = []
        for i in range(0,4):
            try:
                tempGpuArray.append(nvmlDeviceGetHandleByIndex(i))
                tempGpuName.append(nvmlDeviceGetName(tempGpuArray[i]))
                print(nvmlDeviceGetName(tempGpuArray[i]))
            except NVMLError_InvalidArgument:
                print("No Nvidia GPU found, index:",i)
        
        for d in tempGpuArray:
            try:
                nvmlDeviceGetUtilizationRates(d)
                tempGpuUtilAvailablity.append(True)
            except NVMLError_NotSupported:
                print("\n"+str(nvmlDeviceGetName(d))+" does not support utilization rates")
                tempGpuUtilAvailablity.append(False)
                
        for r in tempGpuArray:
            totalVram = nvmlDeviceGetMemoryInfo(r).total
            tempGpuVram.append(int(totalVram))
            print(nvmlDeviceGetName(r),"has Vram:",totalVram,"\n")
            
        array = numpy.array([tempGpuName,tempGpuArray,tempGpuUtilAvailablity,tempGpuVram])
        return array
        
    def getSystem():
        """Gets OS name and version, processor name and total RAM
        
        Arguments
        ----------
        None
        
        Returns
        ----------
        list with all retrived variables
        
        Modules
        ----------
        platform - for OS info
        
        psutil - for system info
        
        Tk Widgets
        ----------
        None
        """
        tempSysArray = []
        uname = platform.uname()
        tempSysArray.append(str(uname.system)+" "+str(uname.version))
        tempSysArray.append(uname.processor)
        tempSysArray.append(psutil.virtual_memory().total)
        for i in tempSysArray:
            print(i)
        return tempSysArray
                      
def startup():
    nvmlInit()
    Data.gpuArray = Data.getGpu()
    Data.systemArray = Data.getSystem()
    print("\n",Data.gpuArray)
    Main().start()
    
startup()