# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 13:26:57 2016

@author: matti_000
"""

import json
import csv

class File():
    """
    Class for data management - saving and loading files, decoding from json format
    
    Functions
    ------------
    
    writeData
    ------------
    
    Writes a given data to a file with a json array
    
    Arguments:
    
    fileName - Name for the file to saved under
    
    data - The data to be written to the file
    
    readData
    ------------
    
    Reads data from a given file with a json array
    
    Arguments:
    
    fileName - Name for file to be read
    """
    
    fields = ["data","delay","loops","memory","notes","date","graphicsPresent"]    
    
    def __init__(self):
        print()
        
    def writeData(fileName,data):
        """
        Writes data to a given file name in a json or csv file, dependant on name
        
        Errors with file name/type should be caught in the UI
        
        Arguments:
    
        fileName - Name for the file to saved under
    
        data - The data to be written to the file
        """
        
        if ".csv" in fileName:
            with open(fileName, "w", newline="") as csvfile:
                writer = csv.writer(csvfile, delimiter=",",quotechar="|",quoting=csv.QUOTE_MINIMAL)
                for field in File.fields:
                    writer.writerow(data[field])
                                        
        else:        
            try: #Try to test whether the user actually selected a file
                with open(fileName, "w") as file:
                    file.write(json.JSONEncoder().encode(data))
                
            except FileNotFoundError: #Except for no file selected - no file name
                return None
        
    def readData(fileName):
        """
        Reads data from a given file with a json array
    
        Arguments:
    
        fileName - Name for file to be read
        
        Returns:
        
        Decoded data from file
        """
        
        
        if ".csv" in fileName:
            with open(fileName, "r", newline="") as csvfile:
                reader = csv.reader(csvfile, delimiter=",", quotechar="|",quoting=csv.QUOTE_MINIMAL)
                rows = []
                for row in reader:
                    rows.append(row)
                
                return {File.fields[0]:rows[0],
                        File.fields[1]:rows[1],
                        File.fields[2]:rows[2],
                        File.fields[3]:rows[3],
                        File.fields[4]:rows[4],
                        File.fields[5]:rows[5]}
        
        else:   
            try:
                with open(fileName) as file: #Try to encode file as json to given file name
                    return json.JSONDecoder().decode(file.read())
                    
            except FileNotFoundError: #Except for no file name/invalid name
                return None
                     
data = {"CPU Utilization":[1,2,3,4,5,6,7,8,9,10],"RAM Utilization":[1,2,3,4,5,6,7,8,9,10]}
#File.writeData("test.json",data)
#print(File.readData("test.json"))
#Retrieval().start()