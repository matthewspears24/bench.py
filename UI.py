# -*- coding: utf-8 -*-
"""
Created on Fri May 13 13:08:51 2016

@author: Matthew
"""

from __future__ import unicode_literals
import sys
import os
import random
import Data
import Retrieval
from PyQt4 import QtGui, QtCore
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np

class BaseGraph(FigureCanvas):
    
    def __init__(self, parent = None, width = 8, height = 5, data = None, animate = False):
        fig = Figure(figsize = (width, height))
        self.axes = fig.add_subplot(111)
        self.axes.set_ylim(0,100)
        self.axes.hold(False)
        
        self.data = self.compute_initial_figure(data)
        print(self.data)
        
        FigureCanvas.__init__(self,fig)
        self.setParent(parent)
        
        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        
        #if animate == True:
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update_figure)
        timer.start(1000)
        
    def compute_initial_figure(self, data):
        if data == None:
            data = []
            for i in range(0,100):
                data.append(0)
            self.axes.plot(data)
        else:
            self.axes.plot(data)
        return data
    
    def update_figure(self):
        del self.data [0]
        self.data.append(random.randint(0,100))
        self.axes.plot(self.data)
    
class Window(QtGui.QMainWindow):
    
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        graph = BaseGraph(animate = True)
        
        self.statusBar()
        
        self.body = Body(self)
        
        self.fileDialog = QtGui.QFileDialog(self)
        
        saveFile = QtGui.QAction(QtGui.QIcon("img/save.png"), "Save Current Test", self)
        saveFile.setShortcut("Ctrl+S")
        saveFile.triggered.connect(lambda: Data.File.writeData(self.showSaveFileDiag(),self.data))
        
        openFile = QtGui.QAction(QtGui.QIcon("img/load.png"), "Load Previous Test", self)
        openFile.setShortcut("Ctrl+O")
        openFile.triggered.connect(lambda: Body.openData(self.body,Data.File.readData(self.showOpenFileDiag())))
        
        createGraph = QtGui.QAction(QtGui.QIcon("img/graph.png"), "Graphs", self)
        createGraph.setShortcut("Ctrl+G")
        createGraph.triggered.connect(lambda: self.showGraphDiag(self.data))
        
        recordData = QtGui.QAction(QtGui.QIcon("img/record.png"), "Record Data", self)
        recordData.setShortcut("Ctrl+R")
        recordData.triggered.connect(lambda: self.showRecordDiag())
        
        self.toolBar = self.addToolBar("File")   
        self.toolBar.addAction(saveFile)     
        self.toolBar.addAction(openFile)
        self.toolBar.addAction(createGraph)
        self.toolBar.addAction(recordData)
        
        self.setWindowTitle("Bench.py")        
        
        self.setCentralWidget(self.body)
        
        self.setGeometry(300,300,500,350)
        
    def showOpenFileDiag(self):

        fileName = self.fileDialog.getOpenFileName(self, "Open Previous Test", "/", "Benchmark Data ( *.json *.csv)")

        try:
            file = open(fileName, 'r')
            
            if ".json" or ".csv" in fileName:
                self.statusBar().showMessage("File Opened")
            else:
                raise FileNotFoundError
            
            return fileName
                
        except FileNotFoundError:
            self.statusBar().showMessage("Invalid File")
            
            return None
            
                
    def showSaveFileDiag(self):
        
        try:
            fileName = self.fileDialog.getSaveFileName(self, "Save File As", "/", "Benchmark Data ( *.json *.csv)")
            self.statusBar().showMessage("File Saved")
            
            return fileName
        
        except FileNotFoundError:
            self.statusBar().showMessage("File Not Saved")
            
            return None
        
    
            
        
    def showGraphDiag(self,data):
        print()
                    
    def showRecordDiag(self):
        dialog = StartRecord()
        if dialog.exec_() and RecordDiag.startRecordOnClose == True:
            Body.openData(Retrieval.Retrieval.startRetrieval(float(dialog.delay.text()),
                                                             int(dialog.loops.text()),
                                                             dialog.notes.text()))           
        
class RecordDiag(QtGui.QWidget):
    startRecordOnClose = False
    def __init__(self,parent = None):
        QtGui.QWidget.__init__(self,parent)
       
    def setupUI(self, dialog):
        dialog.setObjectName("Record Data")
        RecordDiag.startRecordOnClose = False
        
        self.grid = QtGui.QGridLayout()
        
        self.delayTitle = QtGui.QLabel("Delay")        
        self.delay = QtGui.QLineEdit()
        
        self.loopsTitle = QtGui.QLabel("Loops")        
        self.loops = QtGui.QLineEdit()
        
        self.notesTitle = QtGui.QLabel("Notes")        
        self.notes = QtGui.QTextEdit()
        
        self.ok = QtGui.QPushButton("Ok", self.onCancel())        
        self.cancel = QtGui.QPushButton("Cancel", self.onStart())
        
        self.buttons = QtGui.QGridLayout()
        self.buttons.addWidget(self.ok,0,0)
        self.buttons.addWidget(self.cancel,1,0)
        
        self.grid.addWidget(self.delayTitle,0,0)
        self.grid.addWidget(self.delay,0,1)
        self.grid.addWidget(self.loopsTitle,1,0)
        self.grid.addWidget(self.loops,1,1)
        self.grid.addWidget(self.notesTitle,2,0)
        self.grid.addWidget(self.notes,2,1)
        self.grid.addLayout(self.buttons,3,0,2,1)
        
    def onStart(self):
        RecordDiag.startRecordOnClose = True
        self.close()    
        
    def onCancel(self):
        self.close()
        
class StartRecord(QtGui.QDialog, RecordDiag):
    def __init__ (self,parent = None):
        QtGui.QDialog.__init__(self,parent)
        self.setupUI(self) 
        
class Body(QtGui.QWidget):
    
    def __init__(self,parent):
        super(Body,self).__init__(parent)
        
        self.grid = QtGui.QGridLayout()
        
        self.infoDateTitle = QtGui.QLabel("Date/Time")
        
        self.infoDate = QtGui.QLineEdit()
        self.infoDate.setReadOnly(True)
        self.infoDate.insert("-")
        
        self.infoCPUUsedTitle = QtGui.QLabel("Avg. CPU Utilization")
        
        self.infoCPUUsed = QtGui.QLineEdit()
        self.infoCPUUsed.setReadOnly(True)
        self.infoCPUUsed.insert("-")

        self.infoMemoryTitle = QtGui.QLabel("Total RAM")
        
        self.infoMemory = QtGui.QLineEdit()
        self.infoMemory.setReadOnly(True)
        self.infoMemory.insert("-")
        
        self.infoMemoryUsedTitle = QtGui.QLabel("Avg. RAM Utilization")
        
        self.infoMemoryUsed = QtGui.QLineEdit()
        self.infoMemoryUsed.setReadOnly(True)
        self.infoMemoryUsed.insert("-")
        
        self.infoDiskUsedTitle = QtGui.QLabel("Avg. Disk Utilization")
        
        self.infoDiskUsed = QtGui.QLineEdit()
        self.infoDiskUsed.setReadOnly(True)
        self.infoDiskUsed.insert("-")
        
        self.infoGPUUsedTitle = QtGui.QLabel("Avg. GPU Utilization")
        
        self.infoGPUUsed = QtGui.QLineEdit()
        self.infoGPUUsed.setReadOnly(True)
        self.infoGPUUsed.insert("-")
        
        self.infoNotesTitle = QtGui.QLabel("Notes")
        
        self.infoNotes = QtGui.QTextEdit()
        self.infoNotes.setReadOnly(True)
        self.infoNotes.insertPlainText("-")
        
        self.grid.addWidget(self.infoDateTitle,0,0)
        self.grid.addWidget(self.infoDate,0,1)
        self.grid.addWidget(self.infoCPUUsedTitle,1,0)
        self.grid.addWidget(self.infoCPUUsed,1,1)
        self.grid.addWidget(self.infoMemoryTitle,2,0)
        self.grid.addWidget(self.infoMemory,2,1)
        self.grid.addWidget(self.infoMemoryUsedTitle,3,0)
        self.grid.addWidget(self.infoMemoryUsed,3,1)
        self.grid.addWidget(self.infoDiskUsedTitle,4,0)
        self.grid.addWidget(self.infoDiskUsed,4,1)
        self.grid.addWidget(self.infoGPUUsedTitle,5,0)
        self.grid.addWidget(self.infoGPUUsed,5,1)
        self.grid.addWidget(self.infoNotesTitle,6,0)
        self.grid.addWidget(self.infoNotes,6,1)
        
        self.setLayout(self.grid)        
        
    def openData(self,data):
        self.infoDate.clear()
        self.infoCPUUsed.clear()
        self.infoMemory.clear()
        self.infoMemoryUsed.clear()
        self.infoDiskUsed.clear()
        self.infoNotes.clear()
        
        CPUTotal = 0
        RAMTotal = 0
        DiskTotal = 0   
        GPUTotal = 0
        
        if data["graphicsPresent"] == True:
            for d in data["data"]:
                CPUTotal += d[0]
                RAMTotal += d[1]
                DiskTotal += d[2]
                                
            self.infoGPUUsed.clear()
            self.infoGPUUsed.insert("-")
            
        else:
            for d in data["data"]:
                CPUTotal += d[0]
                RAMTotal += d[1]
                DiskTotal += d[2]
                                
            self.infoGPUUsed.clear()
            self.infoGPUUsed.insert("-")
                
        self.infoDate.insert(data["date"])
        self.infoCPUUsed.insert(str(CPUTotal/len(data["data"])))
        self.infoMemory.insert(data["memory"])
        self.infoMemoryUsed.insert(str(RAMTotal/len(data["data"])))
        self.infoDiskUsed.insert(str(DiskTotal/len(data["data"])))
        self.infoNotes.insert(data["notes"])
        

        
qApp = QtGui.QApplication(sys.argv)

aw = Window()
#aw.setWindowFlags(QtCore.Qt.FramelessWindowHint)
aw.show()
sys.exit(qApp.exec_())