# -*- coding: utf-8 -*-
"""
Created on Fri Feb  5 10:18:23 2016

@author: matti_000
"""

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import psutil

def plotGraph(state):
    """
    Plots a live graph from a given field
    
    States
    ----------
    0 - CPU
    1 - RAM
    2 - Disk
    3 - GPU (UNFINISHED)
    """
    Data = [] #array for shown data
    for i in range(0,26): #creates a data set of 25 zeros
        Data.append(0)
            
    fig, ax = plt.subplots()
    line, = ax.plot(Data) #initalization of the graph axis
    ax.set_ylim(0,100) #limit for the y-axis of the graph
    
    def updateGraph(data): #function to set graph data when updating
        line.set_ydata(data)
        return line,
        
    def updateData(baseData): #modifies the data with new information
        while True: #loops forever
            del Data[0] #deletes first element
            if state == 0: #CPU util
                Data.append(psutil.cpu_percent())
            elif state == 1: #RAM util
                Data.append(psutil.virtual_memory().percent)
            yield Data #reutrns data without closing function
            
    ani = animation.FuncAnimation(fig, updateGraph, lambda: updateData(Data), interval = 1000)
    #sets updating animation
    # -fig: the graph to plot on
    # -updategraph: changes the data shown on the graph
    # -lambda: updatedata(Data): changes the source data for the graph
    # -interval: the rate that the graph updates, milliseconds
    
    plt.show()
    #opens the graph window
    
plotGraph(0)