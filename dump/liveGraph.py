# -*- coding: utf-8 -*-
"""
Created on Fri May  6 13:13:26 2016

@author: Matthew
"""

import numpy as np
import psutil
from matplotlib.figure import Figure
from matplotlib.animation import TimedAnimation
from matplotlib.lines import Line2D

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas

class LiveGraph(FigureCanvas, TimedAnimation):
    
    def __init__(self):
        """
        Creates a graph and basic x and y data for it
        
        Also initates animation for the graph with a 1 second interval
        """
        self.n = np.linspace(0,100,100)

        tempArray = []
        for i in range (0,100):
            tempArray.append(0)            
        self.y = tempArray
      
        self.fig = Figure(figsize=(4,2.5), dpi = 64.5)
        ax1 = self.fig.add_subplot(111)
        
        self.line1 = Line2D([],[], color='magenta')
        ax1.add_line(self.line1)
        ax1.set_xlim(0, 100)
        ax1.set_ylim(0, 100)
        
        FigureCanvas.__init__(self, self.fig)
        TimedAnimation.__init__(self, self.fig, interval = 1000)
        
    def _draw_frame(self,framedata):
        """
        Changes the last element of the y data to the current CPU util according to psutil
        """
        del self.y[0]
        util = psutil.cpu_percent(interval = 0.5)
        self.y.append(util)
        self.line1.set_data(self.n,self.y)
        self._drawn_artists = [self.line1]
        
    def new_frame_seq(self):
        return iter(self.y)
    
    def _init_draw(self):
        """
        Draws the inital figure
        """
        lines = [self.line1]
        for l in lines:
            l.set_data([],[])