# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 13:30:11 2016

@author: Matthew
"""

import sys
from PyQt4 import QtGui, QtCore
import numpy as np
from liveGraph import LiveGraph as Graph

class LiveGraph(QtGui.QWidget):
    """
    Creates a QWidget window for the graph
    """
    def __init__(self):
        super(LiveGraph, self).__init__()
        QtGui.QWidget.__init__(self, None, QtCore.Qt.WindowStaysOnTopHint)
        
        self.initUI()
        
    def initUI(self):
        """
        Add the graph widget to the layout and the layout to the window
        """
        self.setWindowTitle("Graph")
        grid = QtGui.QGridLayout()
        self.setLayout(grid)
        
        canvas = Graph()
        
        grid.addWidget(canvas,0,0)
        
        LiveGraph.show(self)
        
        
def main():
    """
    Creates the application instance
    
    Also set the window as borderless
    """
    app = QtGui.QApplication(sys.argv)
    widget = LiveGraph()
    
    pixmap = QtGui.QPixmap(widget.size())
    pixmap.fill(QtCore.Qt.transparent)
    painter = QtGui.QPainter(pixmap)
    painter.setBrush(QtCore.Qt.black)
    painter.drawRoundedRect(pixmap.rect(),0,100)
    painter.end()		
    widget.setMask(pixmap.mask())
    
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()